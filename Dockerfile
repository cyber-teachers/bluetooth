FROM ubuntu:xenial

# Update apt
RUN apt-get update --fix-missing
RUN apt-get install -qqy apt-utils
RUN apt-get dist-upgrade -y

# Install some standard stuff
RUN apt-get install -qqy sudo net-tools htop
RUN apt-get install -qqy ssh openssh-server
RUN apt-get install -qqy git gitg
RUN apt-get install -qqy emacs gedit
RUN apt-get install -qqy udev
RUN apt-get install -y bash-completion usbutils

## Install pybombs
RUN apt-get install -y python-apt python-pip
RUN pip install --upgrade pip
RUN pip install pybombs

RUN apt-get install -y cmake
# Create new user and set password
RUN useradd -ms /bin/bash demo
RUN echo 'demo:demo' | chpasswd
RUN echo 'root:demo' | chpasswd
RUN usermod -aG sudo demo
RUN echo "demo ALL=(ALL:ALL) NOPASSWD: ALL" > /etc/sudoers.d/demo
RUN chown -R demo:demo /home/demo

# Set up X11 configuration
RUN apt-get install -qqy x11-apps
RUN sudo echo "X11UseLocalhost no" >> /etc/ssh/sshd_config

# Get ubertooth

#RUN apt-get build-dep -y ubertooth

# Install crackle dependencies
RUN apt-get install wget bzip2 -y
RUN apt-get install gcc python -y

RUN apt-get install -y wireshark-dev
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y wireshark



RUN ldconfig

# Get crackle

USER demo
WORKDIR /home/demo
RUN git clone https://github.com/mikeryan/crackle.git
WORKDIR /home/demo/crackle
RUN make
USER root
RUN make install

RUN apt-get install libusb-1.0 bluez
# Build libbtbb (for ubertooth)
USER demo
WORKDIR /home/demo
RUN git clone https://github.com/greatscottgadgets/libbtbb.git
WORKDIR /home/demo/libbtbb
RUN mkdir build
WORKDIR /home/demo/libbtbb/build
RUN cmake ..
RUN make
USER root
RUN make install

# Build ubertooth
USER demo
WORKDIR /home/demo
RUN git clone https://github.com/greatscottgadgets/ubertooth.git
WORKDIR /home/demo/ubertooth/host
RUN mkdir build
WORKDIR /home/demo/ubertooth/host/build
RUN cmake ..
RUN make
USER root
RUN make install

RUN apt-get install -y xterm

USER root
EXPOSE 22
ENTRYPOINT service ssh restart && chown -R demo:demo /home/demo/working && /bin/bash
