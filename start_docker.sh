networkname=cybernet


networkrunning=`sudo docker network ls | grep $networkname | wc -l`

if [ "x$networkrunning" == "x0" ]; then
    echo "need to start docker network"
    sudo docker network create $networkname
fi

id=`sudo docker container ls -a | grep bluetooth | sed 's/ .*//'`

if [ "x$id" != "x" ]; then
    echo -n "Docker container bluetooth already exists, should I kill the old container [y/N]: "
    read ret
    if [ "x$ret" == "xy" ] || [ "x$ret" == "xY" ]; then
	echo "removing container"
	sudo docker rm -f $id
    elif [ "x$ret" == "xn" ] || [ "x$ret" == "xN" ]; then
	echo -n ""
    elif [ "x$ret" == "x" ]; then
	 exit
    else
	echo "answer not recognized ... exiting"
    fi
fi

sudo docker run -d -p 7022:22 --name=bluetooth --privileged -v /dev/bus/usb:/dev/bus/usb -v $PWD/working:/home/demo/working --net="$networkname" -i -t bluetooth


id=`sudo docker container ls -a | grep bluetooth | wc -l`


if [ "x$id" == "x0" ]; then
    echo "ERROR: container did not start"
    exit
fi

num=`cat ~/.ssh/config | grep bluetooth | wc -l`

if [ "x$num" == "x0" ]; then
    echo "Adding ssh config for tpms"
    echo "Host bluetooth" >> ~/.ssh/config
    echo "     HostName 127.0.0.1" >> ~/.ssh/config
    echo "     User demo" >> ~/.ssh/config
    echo "     Port 7022" >> ~/.ssh/config
fi

echo "Container created"
echo "To log in to the container run 'ssh -X bluetooth'"


